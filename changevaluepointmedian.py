import bpy
from bpy import context
import builtins as __builtin__

def console_print(*args, **kwargs):
    for a in context.screen.areas:
        if a.type == 'CONSOLE':
            c = {}
            c['area'] = a
            c['space_data'] = a.spaces.active
            c['region'] = a.regions[-1]
            c['window'] = context.window
            c['screen'] = context.screen
            s = " ".join([str(arg) for arg in args])
            for line in s.split("\n"):
                bpy.ops.console.scrollback_append(c, text=line)

def print(*args, **kwargs):
    """Console print() function."""

    console_print(*args, **kwargs) # to py consoles
    __builtin__.print(*args, **kwargs) # to system console

for ob in bpy.data.objects:
    if ob.type == 'MESH':
        if ob.name != 'Armature' and ob.name != 'RootNode':
            mesh = bpy.data.meshes[ob.data.name]    
            print(mesh.vertices)
            for v in mesh.vertices:
                v.co.x =  0.0
                v.co.y =  0.0
                v.co.z =  0.0
                